﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Catalog.API.Models;
using Catalog.API.Repo.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly IProductRepo _repo;
        private readonly ILogger<CatalogController> _logger;

        public CatalogController(IProductRepo repo, ILogger<CatalogController> logger)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Product>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            var products = await _repo.GetProducts();
            return Ok(products);
        }

        [HttpGet("{id}", Name = "GetProduct")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Product>> GetProduct(string id)
        {
            var product = await _repo.GetProduct(id);

            if (product == null)
            {
                _logger.LogError("Product with ID : {id} Not Found", id);
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Product>> CreateProduct([FromBody] Product product)
        {
            //actually not checking the product, now going to check if name is null
            if (product.Name == null)
            {
                _logger.LogError("Bad Request : Product Name cannot be null");
                return BadRequest();
            }
            else
            {
                await _repo.Create(product);
                return CreatedAtRoute("GetProduct", new { id = product.Id }, product);
            }
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<String>> UpdateProduct([FromBody] Product product)
        {
            if (await _repo.Update(product))
            {
                return Ok("Product Updated");
            }
            else
            {
                _logger.LogError("Bad Request : Object not updated");
                return BadRequest();   //when id doesn't match this is returned, there should be a validation for that seperately
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<String>> DeleteProduct(string id)
        {
            var product = GetProduct(id);
            if(product == null)   //if product.Id != id
            {
                _logger.LogError("Delete Failed : Product with ID {id} does not exist", id);
                return NotFound();
            }
            else
            {
                await _repo.Delete(id);
                return Ok("Product with ID " + id + " has been deleted");   //method Ok can't take two arguments
            }
        }

        //the last two not tested yet
        [Route("[action]/{category}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<Product>>> GetProductsByCategory(string category)
        {
            var products = await _repo.GetProductsByCategory(category);

            if(products == null)
            {
                _logger.LogError("Products in Category : {category} Not Found", category);
                return NotFound();
            }

            return Ok(products);
        }
        [Route("[action]/{name}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<Product>>> GetProductsByName(string name)
        {
            var products = await _repo.GetProductsByName(name);

            if (products == null)
            {
                _logger.LogError("Products with Name : {name} Not Found", name);
                return NotFound();
            }
            return Ok(products);
        }
    }
}
