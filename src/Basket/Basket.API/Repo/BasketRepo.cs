﻿using Basket.API.Data.Interfaces;
using Basket.API.Models;
using Basket.API.Repo.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Basket.API.Repo
{
    public class BasketRepo : IBasketRepo
    {
        private readonly IBasketContext _context;

        public BasketRepo(IBasketContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<bool> DeleteBasket(string userName)
        {
            return await _context.Redis.KeyDeleteAsync(userName);
        }

        public async Task<BasketCart> GetBasket(string userName)
        {
            var basket = await _context.Redis.StringGetAsync(userName);

            if (basket.IsNullOrEmpty)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<BasketCart>(basket);
        }

        public async Task<BasketCart> UpdateBasket(BasketCart basket)
        {
            var updatedBasket = await _context.Redis.StringSetAsync(basket.Username, JsonConvert.SerializeObject(basket));
            
            if(!updatedBasket)
            {
                return null;
            }

            return await GetBasket(basket.Username);
        }
    }
}
