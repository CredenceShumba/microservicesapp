﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basket.API.Models
{
    public class BasketCart
    {
        #region Properties
        public string Username { get; set; }

        public List<BasketCartItem> Items { get; set; } = new List<BasketCartItem>();
        #endregion

        #region Constructors
        public BasketCart(){}       //empty constructor

        public BasketCart(string username)
        {
            Username = username;
        }
        #endregion

        #region Logical Methods
        public decimal TotalPrice
        {
            get
            {
                decimal totalPrice = 0;

                foreach(var item in Items)
                {
                    totalPrice += item.Price * item.Quantity;
                }

                return totalPrice;
            }
        }
        #endregion
    }
}
